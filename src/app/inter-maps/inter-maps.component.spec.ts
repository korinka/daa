import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterMapsComponent } from './inter-maps.component';

describe('InterMapsComponent', () => {
  let component: InterMapsComponent;
  let fixture: ComponentFixture<InterMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
