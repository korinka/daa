import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GeneratorComponent } from './generator/generator.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FoodComponent } from './food/food.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LocalComponent } from './local/local.component';
import { RezervationComponent } from './rezervation/rezervation.component';
import { InterMapsComponent} from './inter-maps/inter-maps.component';
import { QrcodeComponent} from './qrcode/qrcode.component';
import { IsLoggedIn } from './isLogged.guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'generator', component: GeneratorComponent, canActivate: [IsLoggedIn] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'food', component: FoodComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [IsLoggedIn] },
  { path: 'local', component: LocalComponent},
  { path: 'rezervation', component: RezervationComponent},
  { path: 'inter_maps', component: InterMapsComponent},
  { path: 'qrcode', component: QrcodeComponent},
  { path: '**', redirectTo: '', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [IsLoggedIn]
})
export class AppRoutingModule { }
